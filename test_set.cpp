#define _USE_MATH_DEFINES
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/features2d.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>
#include <algorithm>
#include <vector>
#include <sys/stat.h>
#include <errno.h>
#include <dirent.h>

using namespace cv;
using namespace std;

//Bounding Box
Mat src;
Mat new_src;
Mat Crop;
Mat CropFin;
Mat Rot;
Mat src_gray, src_dil, src_erod, src_hist, src_dil1, src_blur, m_blur;

Mat threshold_output;
Mat threshold_output1;
Mat dst, dst_dil, dst_gray;

int win_height, win_width;
int dev_width, dev_height;

//parameters
int srcdil_size = 1;
int canny_thresh = 6;
int hough_thresh = 14;
int hough_min = 8;
int hough_max = 9;
int dil_size = 2;
int erod_size = 4;
int median_size = 9;

//operator
Mat dil_element = getStructuringElement( MORPH_RECT, Size( 2*dil_size + 1, 2*dil_size + 1 ), Point( dil_size, dil_size ) );
Mat erod_element = getStructuringElement( MORPH_RECT, Size( 2*erod_size + 1, 2*erod_size + 1), Point( erod_size, erod_size ) );
int median_element = 2*median_size + 1;
int canny_element = 10*canny_thresh;
int hough_element1 = 10*hough_thresh;
int hough_element2 = 10*hough_min;
int hough_element3 = 10*hough_max;
Mat srcdil_element = getStructuringElement( MORPH_RECT, Size( 2*srcdil_size + 1, 2*srcdil_size + 1 ), Point( srcdil_size, srcdil_size ) );

//devide
Rect CRO;
Mat DEVIDE;
int dev;

int min_vote;
RNG rng(0);

//MSER
Mat inImg, textImg;

/// Function header
void devide(int, void*);

void thresh_callback(int, void*);

void Sharpen(const Mat& myImage, Mat& Result)
{
    CV_Assert(myImage.depth() == CV_8U);  // accept only uchar images
    
    Result.create(myImage.size(), myImage.type());
    const int nChannels = myImage.channels();
    
    for (int j = 1; j < myImage.rows - 1; ++j)
    {
        const uchar* previous = myImage.ptr<uchar>(j - 1);
        const uchar* current = myImage.ptr<uchar>(j);
        const uchar* next = myImage.ptr<uchar>(j + 1);
        
        uchar* output = Result.ptr<uchar>(j);
        
        for (int i = nChannels; i < nChannels * (myImage.cols - 1); ++i)
        {
            *output++ = saturate_cast<uchar>(5 * current[i]
                                             - current[i - nChannels] - current[i + nChannels] - previous[i] - next[i]);
        }
    }
    
    Result.row(0).setTo(Scalar(0));
    Result.row(Result.rows - 1).setTo(Scalar(0));
    Result.col(0).setTo(Scalar(0));
    Result.col(Result.cols - 1).setTo(Scalar(0));
}

string folderPath = "/Users/kwangminkim/Desktop/untitled folder/123/";			//저장할 폴더
string dirName= "/Users/kwangminkim/Desktop/untitled folder/img_file/";			//이미지 불러올 폴더
const char *Name;

/** @function main */
int main(int argc, char* argv[])
{
    
    DIR *dir;
    dir = opendir(dirName.c_str());
    string imgName;
    struct dirent *ent;
    if (dir != NULL) {
        while ((ent = readdir (dir)) != NULL) {
            imgName= ent->d_name;
            if(imgName.compare(".")!= 0 && imgName.compare("..")!= 0 && imgName.compare(".DS_Store")!= 0)
            {
                string aux;
                aux.append(dirName);
                aux.append(imgName);
                
                vector<char> udir(folderPath.begin(), folderPath.end());
                vector<char> mkdr(imgName.begin(), imgName.end());
                udir.push_back('\0');
                mkdr.push_back('\0');
                char* st = &udir[0];
                char* nd = &mkdr[0];
                strcat(st, nd);
                
                Name = st;
                
                src= imread(aux);
                win_height = src.rows;
                win_width = src.cols;
                devide(0, 0);
                //thresh_callback(0, 0);
            }
        }
        closedir (dir);
    } else {
        cout<<"not present"<<endl;
    }
    return(0);
}


/** @devide **/

void devide(int, void*)
{
    cvtColor(src, src_gray, CV_BGR2GRAY);
    
    vector<int> rec1;
    vector<vector<Point> > contours1;
    vector<Rect> bboxes1;
    Ptr<MSER> mser = MSER::create(15, (int)(0.00002*src_gray.cols*src_gray.rows), (int)(0.05*src_gray.cols*src_gray.rows), 1, 0.7);
    mser->detectRegions(src_gray, contours1, bboxes1);
    for (int i = 0; i <bboxes1.size();)
    {
        if( bboxes1[i].size().width > 50 || bboxes1[i].size().height > 50 || bboxes1[i].size().width < 20 || bboxes1[i].size().height < 20)
        {
            bboxes1.erase(bboxes1.begin() + i );
            continue;
        }
        
        rectangle(src,bboxes1[i],CV_RGB(0,255,0), 10);
        i++;
    }
    
    dilate(src_gray, src_dil, dil_element);
    erode(src_dil, src_erod, erod_element);
    medianBlur(src_erod, m_blur, median_element);
    Sharpen(m_blur, src_blur);
    Canny(src_blur, threshold_output, canny_element, canny_element * 3, 3, false);
    Sharpen(threshold_output, threshold_output1);
    cvtColor(threshold_output1, dst, CV_GRAY2BGR);
    dilate(dst, dst_dil, srcdil_element);
    cvtColor(dst_dil, dst_gray, CV_BGR2GRAY);
    
    vector<Vec4i> lines;
    HoughLinesP(dst_gray, lines, 1, CV_PI / 180, hough_element1, hough_element2, hough_element3);
    
    /// Apply the erosion operation
    
    vector<int> crs_point;
    Vec4i l;
    
    for (size_t i = 0; i < lines.size(); i++)
    {
        l = lines[i];
        int dx = l[2] - l[0];
        int dy = l[3] - l[1];
        double degree;
        double rad = atan2(dx, dy);
        degree = (rad * 180) / M_PI;
        
        if (degree >= 180) degree -= 180;
        if ( 85 < degree && degree < 95 ) {
            
            if (degree > 90)
            {
                swap(l[0], l[2]);
                swap(l[1], l[3]);
            }
            
            //		line(src, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255, 0, 0), 3, 8);
            
            crs_point.push_back(l[0]);
            crs_point.push_back(l[1]);
            crs_point.push_back(l[2]);
            crs_point.push_back(l[3]);
        }
        
    }
    
    
    int crs_min1;
    int srta, srtb;
    int temp1 = 0;
    for (srta = 0; srta < crs_point.size(); srta = srta + 4)
    {
        crs_min1 = crs_point[srta + 1];
        for (srtb = srta; srtb < crs_point.size() - 4; srtb = srtb + 4)
        {
            if (crs_min1 > crs_point[srtb + 5]) {
                crs_min1 = crs_point[srtb + 5];
                temp1 = srtb + 4;
            }
        }
        if (crs_point[srta + 1] != crs_min1)
        {
            crs_point[temp1 + 1] = crs_point[srta + 1];
            crs_point[srta + 1] = crs_min1;
            swap(crs_point[temp1], crs_point[srta]);
            swap(crs_point[temp1 + 2], crs_point[srta + 2]);
            swap(crs_point[temp1 + 3], crs_point[srta + 3]);
        }
    }
    
    vector<float> crs_2pnt;
    float crs_d_y1;
    float crs_d_y2;
    
    for (int h = 0; h < crs_point.size() - 4; h = h + 4)
    {
        //	cout << crs_point[h] << ' ' << crs_point[h + 1] << ' ' << crs_point[h + 2] << ' ' << crs_point[h + 3] << '\n';
        
        double dx = crs_point[h + 2] - crs_point[h];
        double dy = crs_point[h + 3] - crs_point[h + 1];
        
        double rad = atan2(dx, dy);
        double degree = (rad * 180) / M_PI;
        double angle = abs(dy/dx);
        
        if (degree >= 180) degree -= 180;
        if(dy == 0) angle = 0;
        
        if (degree > 88 && degree < 92)
        {
            
            crs_d_y1 = floor((angle * (-crs_point[h])) + crs_point[h + 1]);
            crs_d_y2 = floor((angle * (win_width - crs_point[h])) + crs_point[h + 1]);
            
            //	arrowedLine(src, Point(0, crs_d_y1), Point(win_width, crs_d_y2), Scalar(255, 0, 0), 3, 8);
            
            //	cout << degree << ' ' << angle << ' ' << crs_d_y1 << ' ' << crs_d_y2 << '\n';
            crs_2pnt.push_back(crs_d_y1);
            crs_2pnt.push_back(crs_d_y2);
        }
        
    }
    int crs_min2;
    int crs_srtc, crs_srtd;
    int crs_temp2 = 0;
    for (crs_srtc = 0; crs_srtc < crs_2pnt.size(); crs_srtc = crs_srtc + 2)
    {
        crs_min2 = crs_2pnt[crs_srtc];
        for (crs_srtd = crs_srtc; crs_srtd < crs_2pnt.size() -2; crs_srtd = crs_srtd + 2)
        {
            if (crs_min2 > crs_2pnt[crs_srtd + 2])
            {
                crs_min2 = crs_2pnt[crs_srtd + 2];
                crs_temp2 = crs_srtd + 2;
            }
            if (crs_2pnt[crs_srtc] != crs_min2)
            {
                crs_2pnt[crs_temp2] = crs_2pnt[crs_srtc];
                crs_2pnt[crs_srtc] = crs_min2;
                swap(crs_2pnt[crs_temp2 + 1], crs_2pnt[crs_srtc + 1]);
            }
        }
        if (crs_srtc != 0 && (crs_2pnt[crs_srtc] - crs_2pnt[crs_srtc - 2] < 1 || crs_2pnt[crs_srtc + 1] - crs_2pnt[crs_srtc - 1] < 1))
        {
            crs_2pnt.erase(crs_2pnt.begin() + crs_srtc);
            crs_2pnt.erase(crs_2pnt.begin() + crs_srtc);
            crs_srtc = crs_srtc - 2;
            continue;
        }
        else if (crs_2pnt[crs_srtc] > win_height || crs_2pnt[crs_srtc + 1] > win_height || crs_2pnt[crs_srtc] < 0 || crs_2pnt[crs_srtc + 1] < 0)
        {
            crs_2pnt.erase(crs_2pnt.begin() + crs_srtc);
            crs_2pnt.erase(crs_2pnt.begin() + crs_srtc);
            crs_srtc = crs_srtc - 2;
            continue;
        }
        int sc;
        double a = (crs_2pnt[crs_srtc + 1] - crs_2pnt[crs_srtc]) / win_width;
        
        for (size_t bxt = 0; bxt < bboxes1.size(); bxt++ )
        {
            double d = a*bboxes1[bxt].x + crs_2pnt[crs_srtc];
            double yh = (bboxes1[bxt].y + bboxes1[bxt].height);
            
            
            if ( d > bboxes1[bxt].y + 10 && d < yh - 10 )
            {
                sc = 255;
                crs_2pnt.erase(crs_2pnt.begin() + crs_srtc, crs_2pnt.begin() + (crs_srtc + 2));
                // cout << d << ' ' << bboxes1[bx] << '\n';
                
                crs_srtc = crs_srtc - 2;
                break;
            }
            sc = 0;
        }
        //line(src, Point(0, crs_2pnt[crs_srtc]), Point(win_width, crs_2pnt[crs_srtc + 1]), Scalar(255, sc, 0), 3, 8);
    }
    
    for (int u = 0; u < crs_2pnt.size();)
    {
        if( u != crs_2pnt.size() - 2 && ((crs_2pnt[u + 2] - crs_2pnt[u]) < 100 || (crs_2pnt[u + 3] - crs_2pnt[u + 1]) < 100))
        {
            crs_2pnt[u + 2] = (crs_2pnt[u] + crs_2pnt[u + 2]) / 2;
            crs_2pnt[u + 3] = (crs_2pnt[u + 3] + crs_2pnt[u + 1]) / 2;
            crs_2pnt.erase(crs_2pnt.begin() + (u), crs_2pnt.begin() + (u + 2));
            continue;
        }
        else
            
            //cout << crs_2pnt[u] << ' ' << crs_2pnt[u + 1] << '\n';
            line(src, Point(0, crs_2pnt[u]), Point(win_width, crs_2pnt[u + 1]), Scalar(255, 0, 0), 3, 8);
        u = u + 2;
    }
    
    // @make directory each imagefiles //
    const int dir = mkdir(Name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (dir == -1)
    {
        printf("Error\n");
        printf("error : %d\n", errno);
        exit(1);
    }
    
    for (dev = 0; dev < crs_2pnt.size() + 2; dev = dev + 2 )
    {
        if(crs_2pnt.size() <= 2)
        {
            DEVIDE = src;
            dev = 4;
        }
        else if(crs_2pnt.size() > 2)
        {
            double y;
            if(dev == 0)
            {
                if(crs_2pnt[0] > crs_2pnt[1])
                {
                    y = crs_2pnt[0];
                }
                else if(crs_2pnt[0] < crs_2pnt[1])
                {
                    y = crs_2pnt[1];
                }
                CRO = Rect(0, 0, win_width, crs_2pnt[dev]);
            }
            else if(dev == crs_2pnt.size())
            {
                if(crs_2pnt[dev - 2] > crs_2pnt [dev - 1])
                {
                    y = crs_2pnt[dev - 1];
                }
                else if(crs_2pnt[dev - 2] <= crs_2pnt[dev - 1])
                {
                    y = crs_2pnt[dev - 2];
                }
                CRO = Rect(0, y, win_width, win_height - y);
            }
            else if(dev != 0 && dev != (crs_2pnt.size()))
            {
                if(crs_2pnt[dev - 2] > crs_2pnt[dev - 1])
                {
                    y = crs_2pnt[dev - 1];
                }
                else if (crs_2pnt[dev - 2] < crs_2pnt[dev - 1])
                {
                    y = crs_2pnt[dev - 2];
                }
                CRO = Rect(0, crs_2pnt[dev - 2], win_width, crs_2pnt[dev] - crs_2pnt[dev - 2]);
            }
            DEVIDE = src(CRO);
        }
        cout << CRO << '\n';
        
        if(DEVIDE.rows < 100)
        {
            continue;
        }
        
        thresh_callback(0, 0);
    }
}

//* @devide

// @function thresh_callback //
void thresh_callback(int, void*)
{
    cvtColor(DEVIDE, src_gray, CV_BGR2GRAY);
    
    dev_width = DEVIDE.cols;
    dev_height = DEVIDE.rows;
    cout << "devide height = " << dev_height << '\n';
    
    cout << src_gray.cols << ' ' << src_gray.rows << '\n';
    
    
    // @text feature find //
    vector<int> rec;
    vector<vector<Point> > contours;
    vector<Rect> bboxes;
    Ptr<MSER> mser = MSER::create(23, (int)(0.00002*src_gray.cols*src_gray.rows), (int)(0.05*src_gray.cols*src_gray.rows), 1, 0.7); // line 잡을 땐 23, box 만들 땐 7
    mser->detectRegions(src_gray, contours, bboxes);
    for (int txt = 0; txt < bboxes.size();)
    {
        
        if( bboxes[txt].size().width > 30 || bboxes[txt].size().height > 30 || bboxes[txt].size().width < 10 || bboxes[txt].size().height < 10)
        {
            bboxes.erase(bboxes.begin() + txt );
            continue;
        }
        
        rec.push_back(bboxes[txt].width);
        cout << txt << ' ' << bboxes[txt] << '\n';
        txt++;
        //      rectangle(src, bboxes[txt], CV_RGB(0, 255, 0));
        
    }
    
    // @edge detected //
    dilate(src_gray, src_dil, dil_element);
    erode(src_dil, src_erod, erod_element);
    medianBlur(src_erod, m_blur, median_element);
    Sharpen(m_blur, src_blur);
    Canny(src_blur, threshold_output, canny_element, canny_element * 3, 3, false);
    Sharpen(threshold_output, threshold_output1);
    cvtColor(src_gray, dst, CV_GRAY2BGR);
    dilate(dst, dst_dil, srcdil_element);
    cvtColor(dst_dil, dst_gray, CV_BGR2GRAY);
    
    
    
    // @houghline detected //
    vector<Vec4i> lines;
    HoughLinesP(threshold_output1, lines, 1, CV_PI / 180, hough_element1, hough_element2, hough_element3);
    
    
    // @find lines degree at : (- 15 < degree < 15)  && sort by degree //
    vector<int> point;
    Vec4i l;
    
    for (size_t i = 0; i < lines.size(); i++)
    {
        l = lines[i];
        int dx = l[2] - l[0];
        int dy = l[3] - l[1];
        double degree;
        double rad = atan2(dx, dy);
        degree = (rad * 180) / M_PI;
        
        if (degree >= 180) degree -= 180;
        if (degree < 15 || degree > 165) {
            
            if (degree > 165)
            {
                swap(l[0], l[2]);
                swap(l[1], l[3]);
            }
        }
        point.push_back(l[0]);
        point.push_back(l[1]);
        point.push_back(l[2]);
        point.push_back(l[3]);
        
    }
    
    // @sort by x1 //
    int min1;
    int srta, srtb;
    int temp1 = 0;
    for (srta = 0; srta < point.size(); srta = srta + 4)
    {
        min1 = point[srta];
        for (srtb = srta; srtb < point.size() - 4; srtb = srtb + 4)
        {
            if (min1 > point[srtb + 4]) {
                min1 = point[srtb + 4];
                temp1 = srtb + 4;
            }
        }
        if (point[srta] != min1)
        {
            point[temp1] = point[srta];
            point[srta] = min1;
            swap(point[temp1 + 1], point[srta + 1]);
            swap(point[temp1 + 2], point[srta + 2]);
            swap(point[temp1 + 3], point[srta + 3]);
        }
        //cout << point[srta] << ' ' << point[srta + 1] << '\n';
    }
    
    // @make y1 & y2 to 0, win_height //
    vector<float> spoint;
    float d_x1;
    float d_x2;
    Rect ROI;
    Rect DTL;
    Rect FIN;
    
    for (int h = 0; h < point.size() - 4; h = h + 4)
    {
        int dx = point[h + 2] - point[h];
        int dy = point[h + 3] - point[h + 1];
        double rad = atan2(dx, dy);
        double degree = (rad * 180) / M_PI;
        double angle = 1 / rad;
        if (degree >= 180) degree -= 180;
        if (degree < 10 || degree > 170)
        {
            d_x1 = floor((dev_height - point[h + 1] + (angle * point[h])) / angle);
            d_x2 = floor((-point[h + 3] + (angle * point[h + 2])) / angle);
            
            spoint.push_back(d_x1);
            spoint.push_back(d_x2);
            //cout << " d_x1 = " << d_x1 << ' ' << "d_x2 = " << d_x2 << '\n';
        }
    }
    
    // @sort by x1 && put close lines together && erase points that out of window //
    int min2;
    int srtc, srtd;
    int temp2 = 0;
    for (srtc = 0; srtc < spoint.size(); srtc = srtc + 2)
    {
        min2 = spoint[srtc];
        for (srtd = srtc; srtd < spoint.size() - 2; srtd = srtd + 2)
        {
            if (min2 > spoint[srtd + 2]) {
                min2 = spoint[srtd + 2];
                temp2 = srtd + 2;
                
            }
            if (spoint[srtc] != min2){
                spoint[temp2] = spoint[srtc];
                spoint[srtc] = min2;
                swap(spoint[temp2 + 1], spoint[srtc + 1]);
            }
            cout << "spoint[srtc] = " << spoint[srtc] << ' ' << "spoint[srtc +1] = " << spoint[srtc + 1] << '\n';
        }
        
        if (srtc != 0 && (spoint[srtc] - spoint[srtc - 2] < 1 || spoint[srtc + 1] - spoint[srtc - 1] < 1)){
            spoint.erase(spoint.begin() + srtc);
            spoint.erase(spoint.begin() + srtc);
            srtc = srtc - 2;
            continue;
        }
        else if (spoint[srtc] > dev_width || spoint[srtc + 1] > dev_width || spoint[srtc] < 0 || spoint[srtc + 1] < 0) {
            spoint.erase(spoint.begin() + srtc);
            spoint.erase(spoint.begin() + srtc);
            srtc = srtc - 2;
            continue;
        }
        
        // @erase lines passing by found feature boxes //
        double a = DEVIDE.rows / ( spoint[srtc] - spoint[srtc + 1] );
        double b = -a * spoint[srtc + 1];
        int sc;
        
        for ( size_t bx = 0; bx < bboxes.size(); bx++ )
        {
            double d = ( bboxes[bx].y - b ) / a;
            double xw = ( bboxes[bx].x + bboxes[bx].size().width );
            
            if ( d > bboxes[bx].x && d <xw )
            {
                sc = 255;
                spoint.erase(spoint.begin() + srtc, spoint.begin() + (srtc + 2));
                srtc = srtc - 2;
                break;
            }
            sc = 0;
        }
        line( DEVIDE, Point(spoint[srtc], dev_height), Point(spoint[srtc + 1], 0), Scalar( 255, 0, 0 ), 3, 8 );
        
        ostringstream name;
        name << Name << "_" << dev << ".jpg";
        imwrite(name.str(), DEVIDE);
        cout << spoint[srtc] << ' ' << spoint[srtc + 1] << '\n';
    }   //라인 작업 끝
    
    
    // @make region from found points //
    for (int c = 0; c < spoint.size() + 2; c = c + 2)
    {
        if (spoint.size() == 0)
        {
            break;
        }
        if (c == 0)
        {
            if (spoint[c] >= spoint[c + 1])
            {
                ROI = Rect(0, 0, spoint[c], dev_height);
            }
            else if (spoint[c] < spoint[c + 1])
            {
                ROI = Rect(0, 0, spoint[c + 1], dev_height);
            }
        }
        else if (c == spoint.size())
        {
            if (spoint[c - 2] < spoint[c - 1])
            {
                if(spoint[c - 2] - 20 < 0)
                {
                    spoint[c - 2] = 0;
                    ROI = Rect(spoint[c - 2] - 20, 0, (dev_width - spoint[c - 2]), dev_height);
                }
                else
                    ROI = Rect(spoint[c - 2] - 20, 0, (dev_width - spoint[c - 2]), dev_height);
            }
            else if (spoint[c - 2] > spoint[c - 1])
            {
                if(spoint[c - 1] - 20 < 0)
                {
                    spoint[c - 1] = 0;
                    ROI = Rect(spoint[c - 1], 0, (dev_width - spoint[c - 2]), dev_height);
                }
                else
                    ROI = Rect(spoint[c - 1] - 20, 0, (dev_width - spoint[c - 1]), dev_height);
            }
        }
        else if ((spoint[c - 2] < spoint[c - 1]) && (spoint[c] < spoint[c + 1]))
        {
            if (spoint[c - 2] < 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c + 1] - spoint[c - 2], dev_height);
            }
            else if (spoint[c - 2] < 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c + 1] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c + 1] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c + 1] - spoint[c - 2] + 40, dev_height);
            }
        }
        else if ((spoint[c - 2] > spoint[c - 1]) && (spoint[c] < spoint[c + 1]))
        {
            if(spoint[c - 1] < 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 1], 0, spoint[c + 1] - spoint[c - 1], dev_height);
            }
            else if(spoint[c - 1] < 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 1], 0, spoint[c + 1] - spoint[c - 1] + 20, dev_height);
            }
            else if(spoint[c - 1] >= 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 1] - 20, 0, spoint[c + 1] - spoint[c - 1] + 20, dev_height);
            }
            else if(spoint[c - 1] >= 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 1] - 20, 0, spoint[c + 1] - spoint[c - 1] + 40, dev_height);
            }
        }
        else if ((spoint[c - 2] < spoint[c - 1]) && (spoint[c] > spoint[c + 1]))
        {
            if (spoint[c - 2] < 20 && dev_width - spoint[c] < 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c] - spoint[c - 2], dev_height);
            }
            else if (spoint[c - 2] < 20 && dev_width - spoint[c] >= 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c] < 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c] >= 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c] - spoint[c - 2] + 40, dev_height);
            }
        }
        else if ((spoint[c - 2]  > spoint[c - 1]) && (spoint[c] > spoint[c + 1]))
        {
            if(spoint[c - 1] < 20 && dev_width - spoint[c] < 40)
            {
                ROI = Rect(spoint[c - 1], 0, spoint[c] - spoint[c - 1], dev_height);
            }
            else if(spoint[c - 1] < 20 && dev_width - spoint[c] >= 40)
            {
                ROI = Rect(spoint[c - 1], 0, spoint[c] - spoint[c - 1] + 20, dev_height);
            }
            else if(spoint[c - 1] >= 20 && dev_width - spoint[c] < 40)
            {
                ROI = Rect(spoint[c - 1] - 20, 0, spoint[c] - spoint[c - 1] + 20, dev_height);
            }
            else if(spoint[c - 1] >= 20 && dev_width - spoint[c] >= 40)
            {
                ROI = Rect(spoint[c - 1] - 20, 0, spoint[c] - spoint[c - 1] + 40, dev_height);
            }
        }
        else if ((spoint[c - 2] = spoint[c - 1]) && (spoint[c] > spoint[c + 1]))
        {
            if (spoint[c - 2] < 20 && dev_width - spoint[c] < 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c] - spoint[c - 2], dev_height);
            }
            else if (spoint[c - 2] < 20 && dev_width - spoint[c] >= 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c] < 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c] >= 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c] - spoint[c - 2] + 40, dev_height);
            }
        }
        else if ((spoint[c - 2] = spoint[c - 1]) && (spoint[c] < spoint[c + 1]))
        {
            if (spoint[c - 2] < 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c + 1] - spoint[c - 2], dev_height);
            }
            else if (spoint[c - 2] < 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c + 1] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c + 1] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c + 1] - spoint[c - 2] + 40, dev_height);
            }
        }
        else if ((spoint[c - 2] > spoint[c - 1]) && (spoint[c] = spoint[c + 1]))
        {
            if(spoint[c - 1] < 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 1], 0, spoint[c + 1] - spoint[c - 1], dev_height);
            }
            else if(spoint[c - 1] < 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 1], 0, spoint[c + 1] - spoint[c - 1] + 20, dev_height);
            }
            else if(spoint[c - 1] >= 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 1] - 20, 0, spoint[c + 1] - spoint[c - 1] + 20, dev_height);
            }
            else if(spoint[c - 1] >= 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 1] - 20, 0, spoint[c + 1] - spoint[c - 1] + 40, dev_height);
            }
        }
        else if ((spoint[c - 2] < spoint[c - 1]) && (spoint[c] = spoint[c + 1]))
        {
            if (spoint[c - 2] < 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c + 1] - spoint[c - 2], dev_height);
            }
            else if (spoint[c - 2] < 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 2], 0, spoint[c + 1] - spoint[c - 2] + 20, dev_height);
            }
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c + 1] < 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c + 1] - spoint[c - 2] + 20, dev_height);
            }
            
            else if (spoint[c - 2] >= 20 && dev_width - spoint[c + 1] >= 40)
            {
                ROI = Rect(spoint[c - 2] - 20, 0, spoint[c + 1] - spoint[c - 2] + 40, dev_height);
            }
        }
        Crop = DEVIDE(ROI);
        
        // @rotate Regions forard lookup //
        if(Crop.cols == 0 || Crop.rows == 0)
        {
            continue;
        }
        
        {
            int dx;
            double Rrad, Rdegree, Rangle, Nrad, Ndegree, Nangle;
            double T, Trad;
            int	a = (Crop.cols / 2);
            int	b = (Crop.rows / 2);
            int	dy = Crop.rows;
            double ld = sqrt(pow(a, 2) + pow(b, 2));
            if (c == 0)
            {
                dx = -(spoint[ c ] - spoint [ c + 1 ]);
                Rrad = atan2(dx, dy);
                Rdegree = (Rrad * 180) / M_PI;
                Rangle = 1 / Rrad;
                Point2f pt(Crop.cols / 2., Crop.rows / 2.);
                
                Mat r = getRotationMatrix2D(pt, Rdegree, 1.0);
                //cout << Crop.cols << ' ' << Crop.rows << '\n';
                warpAffine(Crop, Rot, r, Size(Crop.cols, Crop.rows));
                
                Nrad = atan2(Crop.rows, Crop.cols);
                Ndegree = (Nrad * 180) /M_PI;
                Nangle = 1 / Nrad;
                
                T = (Ndegree - fabs(Rdegree));
                Trad = (T * M_PI) / 180;
                double X = (ld * cos(Trad));
                double W = abs(X - a);
                double col = Crop.cols - (2*W);
                
                if ( col < 40)
                {
                    continue;
                }
                DTL = Rect(W, 0, col, Crop.rows);
                inImg = Rot(DTL);
            }
            else if (c == spoint.size())
            {
                dx = -(spoint[c - 2] - spoint [c - 1]);
                Rrad = atan2(dx, dy);
                Rdegree = (Rrad * 180) / M_PI;
                Rangle = 1 / Rrad;
                Point2f pt(Crop.cols / 2., Crop.rows / 2.);
                
                Mat r = getRotationMatrix2D(pt, Rdegree, 1.0);
                warpAffine(Crop, Rot, r, Size(Crop.cols, Crop.rows));
                
                Nrad = atan2(Crop.rows, Crop.cols);
                Ndegree = (Nrad * 180) /M_PI;
                Nangle = 1 / Nrad;
                
                T = (Ndegree - fabs(Rdegree));
                Trad = (T * M_PI) / 180;
                double X = (ld * cos(Trad));
                double W = abs(X - a);
                double col = Crop.cols - (2*W);
                
                if ( col < 40 )
                {
                    continue;
                }
                DTL = Rect(W, 0, col, Crop.rows);
                inImg = Rot(DTL);
            }
            else if (c != 0  && c != spoint.size())
            {
                dx = -(spoint[c - 2] - spoint[c - 1]);
                Rrad = atan2(dx, dy);
                Rdegree = (Rrad * 180) / M_PI;
                Rangle = 1 / Rrad;
                Point2f pt(Crop.cols / 2., Crop.rows / 2.);
                
                Mat r = getRotationMatrix2D(pt, Rdegree, 1.0);
                warpAffine(Crop, Rot, r, Size(Crop.cols, Crop.rows));
                
                Nrad = atan2(Crop.rows, Crop.cols);
                Ndegree = (Nrad * 180) /M_PI;
                Nangle = 1 / Nrad;
                
                T = (Ndegree - fabs(Rdegree));
                Trad = (T * M_PI) / 180;
                double X = (ld * cos(Trad));
                double W = X - a;
                double col = Crop.cols - (2*W);
                
                if ( col < 60 )
                {
                    continue;
                }
                DTL = Rect(W , 0, col, Crop.rows);
                inImg = Rot(DTL);
            }
            
            // @find text feature //
            cvtColor(inImg, textImg, CV_BGR2GRAY);
            vector<int> rec;
            vector<vector<Point> > contours;
            vector< Rect> bboxes;
            Rect BOX;
            
            Ptr< MSER> mser = MSER::create(13, (int)(0.00002*textImg.cols*textImg.rows), (int)(0.05*textImg.cols*textImg.rows), 1, 0.7);
            mser->detectRegions(textImg, contours, bboxes);
            
            
            for (int i = 0; i < bboxes.size(); i++)
            {
                
                if( bboxes[i].size().width > 60 || bboxes[i].size().height > 60)
                {
                    bboxes.erase(bboxes.begin() + i );
                    continue;
                }
                else if ( bboxes[i].size().width < 10 || bboxes[i].size().height < 10)
                {
                    bboxes.erase(bboxes.begin() + i );
                    continue;
                }
                //	rectangle(inImg, bboxes[i], CV_RGB(0, 255, 0));
                
                rec.push_back(bboxes[i].x - 2);
                rec.push_back(bboxes[i].y);
                rec.push_back(bboxes[i].size().width + 4);
                rec.push_back(bboxes[i].size().height);
            }
            
            // @sort by x1 //
            int min3;
            int srte, srtf;
            int temp3 = 0;
            for (srte = 0; srte < rec.size(); srte = srte + 4)
            {
                min3 = rec[srte];
                for (srtf = srte; srtf < rec.size() - 4; srtf = srtf + 4)
                {
                    if (min3 > rec[srtf + 4])
                    {
                        min3 = rec[srtf + 4];
                        temp3 = srtf + 4;
                    }
                }
                if (rec[srte] != min3)
                {
                    rec[temp3] = rec[srte];
                    rec[srte] = min3;
                    swap(rec[temp3 + 1], rec[srte + 1]);
                    swap(rec[temp3 + 2], rec[srte + 2]);
                    swap(rec[temp3 + 3], rec[srte + 3]);
                }
                //cout << c << '*' << ' ' << rec[srte] << ' ' << rec[srte + 1] << ' ' << rec[srte + 2] <<  ' ' << rec[srte + 3] << '\n';
            }
            
            // @make text region //
            for (size_t q = 0; q < rec.size(); q = q + 4)
            {
                if(q > 3)
                {
                    if( rec[q - 3] > rec[q + 1] )
                    {
                        if( rec[q] <= (rec[q - 4] + rec[q - 2]) && (rec[q - 4] + rec[q - 2]) < (rec[q] + rec[q + 2]) )
                        {
                            swap(rec[q - 3], rec[q + 1]);
                            rec.insert( rec.begin() + (q - 2), ((rec[q] + rec[q + 2]) - rec[q - 4]) );
                            if((rec[q + 2] + rec[q]) > (rec[q - 3] + rec[q + 4])) // (-3) + (-1) > 1 + 3
                            {
                                rec.insert(rec.begin() + (q - 1), ((rec[q + 2] + rec[q]) - rec[q - 3])); // (q - 1) = ((-3) + (-1)) - 1
                                rec.erase(rec.begin() + q, rec.begin() + (q + 6));
                            }
                            else if((rec[q + 2] + rec[q]) <= (rec[q - 3] + rec[q + 4])) // (-3) + (-1) <= 1 + 3
                            {
                                swap(rec[q], rec[q + 4]); // q - 1 , q + 3
                                rec.erase(rec.begin() + q, rec.begin() + (q + 5));
                            }
                            q = 0;
                            continue;
                        }//ok
                        else if( rec[q] <= (rec[q - 4] + rec[q - 2]) && (rec[q - 4] + rec[q - 2]) >= (rec[q] + rec[q + 2]) )
                        {
                            swap(rec[q - 3], rec[q + 1]);
                            if((rec[q + 1] + rec[q - 1]) > (rec[q - 3] + rec[q + 3]) ) // (-3) + (-1) > 1 + 3
                            {
                                rec.insert(rec.begin() + (q - 1), (( rec[q + 1] + rec[q - 1] ) - rec[q - 3]) ); // (q - 1) = ((-3) + (-1)) - 1
                                rec.erase(rec.begin() + q, rec.begin() + (q + 5));
                            }
                            else if((rec[q + 1] + rec[q - 1]) <= (rec[q - 3] + rec[q + 3]) )
                            {
                                swap(rec[q - 1], rec[q + 3]);
                                rec.erase(rec.begin() + q, rec.begin() + (q + 4));
                            }
                            q = 0;
                            continue;
                        }
                    }
                    else if( rec[q - 3] <= rec[q + 1] )
                    {
                        if ( rec[q] <= (rec[q - 4] + rec[q - 2]) && (rec[q - 4] + rec[q - 2]) < (rec[q] + rec[q + 2]))
                        {
                            rec.insert(rec.begin() + (q - 2), ((rec[q] + rec[q + 2]) - rec[q - 4]));
                            if((rec[q - 3] + rec[q]) < (rec[q + 2] + rec[q + 4]))
                            {
                                rec.insert(rec.begin() + (q - 1), (rec[q + 2] + rec[q + 4]) - rec[q - 3]);
                                rec.erase(rec.begin() + q, rec.begin() + (q + 6));
                            }
                            else if((rec[q - 3] + rec[q]) >= (rec[q + 2] + rec[q + 4]))
                            {
                                swap(rec[q - 1], rec[q]);
                                rec.erase(rec.begin() + q, rec.begin() + (q + 5));
                            }
                            q = 0;
                            continue;
                        }
                        else if ( rec[q] <= (rec[q - 4] + rec[q - 2]) && (rec[q - 4] + rec[q - 2]) >= (rec[q] + rec[q + 2]) )
                        {
                            if((rec[q - 3] + rec[q - 1]) < (rec[q + 1] + rec[q + 3]))
                            {
                                rec.insert(rec.begin() + (q - 1), (rec[q + 1] + rec[q + 3]) - rec[q - 3]);
                                rec.erase(rec.begin() + q, rec.begin() + (q + 5));
                            }
                            else if ((rec[q - 3] + rec[q - 1]) >= (rec[q + 1] + rec[q + 3]))
                            {
                                rec.erase(rec.begin() + q, rec.begin() + (q + 4));
                            }
                            q = 0;
                            continue;
                        }
                    }
                }
                //	cout << c << ' ' << rec[q - 4] << ' ' << rec[q - 3] << ' ' << rec[q - 2] << ' ' << rec[q - 1] << '\n';
            }
            
            // @erase text region smaller than 3000 //
            for (size_t y = 0; y < rec.size(); y = y + 4)
            {
                
                if ((rec[y + 2] * rec[y + 3]) < 3000)
                {
                    rec.erase(rec.begin() + y, rec.begin() + (y + 4));
                    y = y - 4;
                    continue;
                }
                
                BOX = Rect( rec[y], rec[y + 1], rec[y + 2], rec[y + 3] );
                //cout << c << ' ' << rec[y] << ' ' << rec[y + 1] << ' ' << rec[y + 2] << ' ' << rec[y + 3] << ' ' << inImg.cols << ' ' << inImg.rows << '\n';
                rectangle(inImg, BOX, CV_RGB(255, 0, 0));
            }
            
            //cout << rec.size() << '\n';
            
            // @devide image between text regions //
            int lwidth, clwidth, crwidth, rwidth, flwidth, frwidth;
            vector<int> temp_f;
            for (b = 0; b < rec.size(); b = b + 4)
            {
                
                if (rec.size() == 4)
                {
                    //cout<< c << ' ' << "N" << '\n';
                    b = 4;
                    CropFin = inImg;
                }
                else if( rec.size() == 8 )
                {
                    //cout<< c << ' ' << "D2" << '\n';
                    clwidth = ((rec[0] + rec[2]) + rec[4]) / 2;
                    if (b == 0)
                    {
                        if( clwidth < 20)
                        {
                            CropFin = inImg;
                        }
                        else if( clwidth >= 20)
                        {
                            FIN = Rect(0, 0, clwidth + 20, inImg.rows);
                            CropFin = inImg(FIN);
                        }
                    }
                    else if (b == 4)
                    {
                        if( clwidth < 20)
                        {
                            continue;
                        }
                        else if( clwidth >= 20)
                        {
                            FIN = Rect(clwidth - 20, 0, (inImg.cols - clwidth) + 20, Crop.rows);
                            CropFin = inImg(FIN);
                        }
                    }
                }
                else if (rec.size() > 8)
                {
                    //cout<< c << ' ' << "D3" << '\n';
                    size_t n = b + 4;
                    lwidth = rec[b] + rec[b + 2];
                    clwidth = rec[n];
                    crwidth = rec[n] + rec[n + 2];
                    flwidth = (lwidth + clwidth) / 2;
                    temp_f.push_back(flwidth);
                    if (rec.size() > 8)
                    {
                        rwidth = rec[n];                                   //(rec[m] + rec[m + 2]) / 2;
                        frwidth = (crwidth + rwidth) / 2;
                        
                        if (b == 0)
                        {
                            if( inImg.cols - flwidth < 20)
                            {
                                FIN = Rect(0, 0, flwidth, inImg.rows);
                                CropFin = inImg(FIN);
                            }
                            else if( inImg.cols - flwidth >= 20)
                            {
                                FIN = Rect(0, 0, flwidth + 20, inImg.rows);
                                CropFin = inImg(FIN);
                            }
                        }
                        else if (b == rec.size() - 4)
                        {
                            if( temp_f[b/4 - 1] < 20 )
                            {
                                FIN = Rect(temp_f[b/4 - 1], 0, inImg.cols - temp_f[b/4 - 1], Crop.rows);
                                CropFin = inImg(FIN);
                            }
                            else if( temp_f[b/4 - 1] >= 20)
                            {
                                FIN = Rect(temp_f[b/4 - 1] - 20, 0, inImg.cols - temp_f[b/4 - 1] + 20, Crop.rows);
                                CropFin = inImg(FIN);
                            }
                        }
                        else if (4 <= b && b < rec.size() - 4)
                        {
                            if( temp_f[b/4 - 1] < 20 && inImg.cols - temp_f[b/4] < 20 )
                            {
                                FIN = Rect(temp_f[b/4 - 1], 0, flwidth - temp_f[b/4 - 1], Crop.rows);
                                CropFin = inImg(FIN);
                            }
                            else if( temp_f[b/4 - 1] >= 20 && inImg.cols - temp_f[b/4] >= 20 )
                            {
                                FIN = Rect(temp_f[b/4 - 1] - 20, 0, flwidth - temp_f[b/4 - 1] + 40, Crop.rows);
                                CropFin = inImg(FIN);
                            }
                            else if( temp_f[b/4 - 1] < 20 && inImg.cols - temp_f[b/4] >= 20 )
                            {
                                FIN = Rect(temp_f[b/4 - 1], 0, flwidth - temp_f[b/4 - 1] + 20, Crop.rows);
                                CropFin = inImg(FIN);
                            }
                            else if( temp_f[b/4 - 1] >= 20 && inImg.cols - temp_f[b/4] < 20 )
                            {
                                FIN = Rect(temp_f[b/4 - 1] - 20, 0, flwidth - temp_f[b/4 - 1] + 20, Crop.rows);
                                CropFin = inImg(FIN);
                            }
                            
                        }
                        
                    }
                    
                }
                ostringstream name;
                name << Name << "/rotated_im_" << setw(4) << setfill('0') << dev << "_" << setw(4) << setfill('0') << c << "_" << setw(4) << setfill('0') << b << ".jpg";
                imwrite(name.str(), CropFin);
            }
        }
        
        
        
    }
}

//rotation ROI



